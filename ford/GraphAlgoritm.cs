﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graph
{
    public class GraphAlgoritm
    {
        //алгоритм Форда
        public double[] Ford(double[,] matrixWeight)
        {
            double[] rowM = new double[matrixWeight.GetLength(1)];

            for (var i = 0; i < matrixWeight.GetLength(0); i++)
            {
                rowM[i] = double.PositiveInfinity;
            }
            rowM[0] = 0;
            for (var i = 0; i < matrixWeight.GetLength(1); i++)
            {
                for (var j = 0; j < matrixWeight.GetLength(1); j++)
                {
                    if (!double.IsPositiveInfinity(matrixWeight[i, j]) &&
                        (rowM[i] + matrixWeight[i, j] < rowM[j] || double.IsPositiveInfinity(rowM[j])))
                    {
                        rowM[j] = rowM[i] + matrixWeight[i, j];
                    }
                }
            }
            for (var i = 0; i < matrixWeight.GetLength(1); i++)
            {
                for (var j = 0; j < matrixWeight.GetLength(1); j++)
                {
                    if (!double.IsPositiveInfinity(matrixWeight[i, j]) &&
                        (rowM[i] + matrixWeight[i, j] < rowM[j] || double.IsPositiveInfinity(rowM[j])))
                    {
                        rowM[j] = rowM[i] + matrixWeight[i, j];
                    }
                }
            }

            return rowM;
        }
      
    }
}
