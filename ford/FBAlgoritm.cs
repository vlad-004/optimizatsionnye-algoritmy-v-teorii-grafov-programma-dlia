﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ford
{
    class FBAlgoritm
    {
        public class Edge
        {
            public int Src, Dest, weight; //src-из вершины;   dest- в вершину;   weight- вес ребра

            public Edge()
            {
                Src = Dest = weight = 0;
            }
        };

        int V, E;
        public Edge[] edge;

        public FBAlgoritm(int v, int e)
        {
            V = v;
            E = e;
            edge = new Edge[e];
            for (var i = 0; i < e; ++i)
                edge[i] = new Edge();
        }

        public string BellmanFord(FBAlgoritm graph, int src)
        {
            int V = graph.V, E = graph.E;
            int[] dist = new int[V];

            // Шаг 1. Инициализация расстояний от src ко всем другим вершинам как INFINITE

            for (var i = 0; i < V; ++i)
                dist[i] = int.MaxValue;
                dist[src] = 0;

            // Шаг 2: Расслабьте все края | V | - 1 раз. 
            // Простой кратчайший путь от src до любой другой вершины может иметь не более | V | - 1 ребра.

            string abcd = "";
            for (var i = 1; i < V; ++i)
            {
                for (var j = 0; j < E; ++j)
                {
                    var u = graph.edge[j].Src;
                    var v = graph.edge[j].Dest;
                    var weight = graph.edge[j].weight;
                    if (dist[u] != int.MaxValue && dist[u] + weight < dist[v])
                    {
                        dist[v] = dist[u] + weight;
                    }
                }
     
            }
            string abc = "";
            string bce = "";
            for (var j = 0; j < E; ++j)
            {
                var u = graph.edge[j].Src;
                var v = graph.edge[j].Dest;
                var weight = graph.edge[j].weight;
                if (dist[u] != int.MaxValue && dist[u] + weight < dist[v])
                {
                    abc = "Граф содержит отрицательный весовой цикл";
                }
            }
            bce = printArr(dist, V, abc);
            return abcd + "\n" + bce;
        }

        string printArr(int[] dist, int V, string abc)
        {
           
            
         //   abc += "\r\nВершина   Расстояние от началной вершины";
            for (var i = 0; i < V; ++i)
            {
                abc += "\r\n";
                abc += "К вершине  " + (i + 1) ;
                if (dist[i]!=int.MaxValue)
                {
                    abc += ",   расстояние от начальной вершины = " + dist[i];    
                }
                else
                {
                    abc += ",   к данной вершине пути нет" ;
                }
                
            }
            return abc;
        }
    }
}