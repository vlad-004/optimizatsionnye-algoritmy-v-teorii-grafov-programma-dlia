﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ford
{
    public class Voln
    {
        private readonly int[,] graph;

// тут ваш код где заполняем мап, где 1- клетка занята, 0 - свободна
        public Voln(int[,] graph)
        {
            this.graph = graph;
        }

        //поиск пути для Графа
        public string SearchWayGraph(int from, int to)
        {
            int dimension = graph.GetLength(0);
            int len = 0;
            from--;
            to--;
            int[,] fronts = new int[dimension, dimension];
            int i, j, k;
            string result = string.Format("Кратчайший путь из {0} в {1}: ", from + 1, to + 1);


            var way = new int[dimension];
            var alreadyMarked = new int[dimension];

            for (i = 0; i < dimension; i++)
            for (j = 0; j < dimension; j++)
                fronts[i, j] = -1;
            for (i = 0; i < dimension; i++)
                alreadyMarked[i] = 0;
            for (i = 0; i < dimension; i++)
                way[i] = -1;

            fronts[0, 0] = from;
            var step = 0; var find = false;


            while (step < dimension && !find)
            {
                i = 0;
                k = 0;
                if (fronts[step, 0] == -1) break;

                while (fronts[step, i] > -1)
                {
                    for (j = 0; j < dimension; j++)
                    {
                        if (graph[fronts[step, i], j] > 0 && alreadyMarked[j] == 0)
                        {
                            alreadyMarked[j] = 1;
                            fronts[step + 1, k] = j;
                            k++;
                        }
                    }
                    i++;
                }

                i = 0;
                while (fronts[step + 1, i] > -1)
                {
                    if (fronts[step + 1, i] == to)
                    {
                        find = true;
                        break;
                    }
                    i++;
                }
                step++;
            }

            if (find)
            {
                way[step] = to;
                for (i = step - 1; i >= 0; i--)
                {
                    for (k = 0; k < dimension; k++)
                    {
                        if (graph[fronts[i, k], way[i + 1]] > 0)
                        {
                            way[i] = fronts[i, k];
                            len += graph[fronts[i, k], way[i + 1]];
                            break;
                        }
                    }
                }
            }

            way = way[0] != -1 ? way.Where(x => x >= 0).Select(x => x).Select(x => x + 1).ToArray() : way;

            if (way[0] == -1)
            {
                result = string.Format("Нет пути из {0} в {1}", from + 1, to + 1);

            }
            else
            {
                for (int c = 0; c < way.Length; c++)
                {

                    if (c < way.Length - 1)
                    {
                        result += string.Format("{0}->", way[c]);

                    }
                    else
                    {
                        result += string.Format("{0};\r\n", way[c]);
                    }

                }
                result += string.Format("Длина маршрута = {0};\r\n", len);
            }



            return result;
        }

        //волновой алгоритм для лабиринта
        public string SearchWayLabirint()
        {
            int size = graph.GetLength(0);
            int x = 0;
            int y = x;
            int x_to = graph.GetLength(0)-1;
            int y_to = x_to;
            int[,,] matrix = new int[size,size,3];
            string res = String.Empty;
            int step;
            bool added=true,result=true;
            List<int[]> ind = new List<int[]>();
            for(int i=0;i<size;i++)
            {
                for(int j=0;j<size;j++)
                {
                    if(graph[i,j]!=0)
                    {
                        matrix[i,j,0] = -2;// занято
                    }
                    else
                    {
                        matrix[i,j,0] = -1;// Мы еще нигде не были
                    }
                }
            }
            matrix[x_to,y_to,0]= 0;// До финиша ноль шагов - от него будем разбегаться
            step = 0; // Изначально мы сделали ноль шагов
	
            // Пока вершины добаляются и мы не дошли до старта

            while(added && matrix[x,y,0]==-1)
            {
                added = false;// Пока что ничего не добавили
                step++;// Увеличиваем число шагов

                for(int i=0;i<size;i++)// Пробегаем по всей карте
                {
                    for(int j=0;j<size;j++)
                    {
                        // Если (i, j) была добавлена на предыдущем шаге
                        // Пробегаем по всем четырем сторонам
                        if(matrix[i,j,0]== step-1)
                        {   int _i,_j;
                            _i=i+1;_j=j;
                            // Если не вышли за пределы карты -  обрабатываем
                            if(_i>=0 && _j>=0 && _i<size && _j<size)
                            {
                                // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем 
                                if(matrix[_i,_j,0]==-1 && matrix[_i,_j,0]!=-2)
                                {
                                    matrix[_i, _j, 0] = step; // Добав-
                                    matrix[_i, _j, 1] = i; // ля-
                                    matrix[_i, _j, 2] = j; // ем
                                    added = true; // Что-то добавили
                                }
                            }
                            _i=i-1;_j=j;
                            // Если не вышли за пределы карты -  обрабатываем
                            if(_i>=0 && _j>=0 && _i<size && _j<size)
                            {
                                // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем 
                                if (matrix[_i, _j, 0] == -1 && matrix[_i, _j, 0] != -2)
                                {
                                    matrix[_i, _j, 0] = step; // Добав-
                                    matrix[_i, _j, 1] = i; // ля-
                                    matrix[_i, _j, 2] = j; // ем
                                    added = true; // Что-то добавили
                                }
                            }
                            _i=i;_j=j+1;
                            // Если не вышли за пределы карты -  обрабатываем
                            if(_i>=0 && _j>=0 && _i<size && _j<size)
                            {
                                // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем 
                                if (matrix[_i, _j, 0] == -1 && matrix[_i, _j, 0] != -2)
                                {
                                    matrix[_i, _j, 0] = step; // Добав-
                                    matrix[_i, _j, 1] = i; // ля-
                                    matrix[_i, _j, 2] = j; // ем
                                    added = true; // Что-то добавили
                                }
                            }
                            _i=i;_j=j-1;
                            // Если не вышли за пределы карты -  обрабатываем
                            if(_i>=0 && _j>=0 && _i<size && _j<size)
                            {
                                // Если (_i, _j) уже добавлено или непроходимо, то не обрабатываем 
                                if (matrix[_i, _j, 0] == -1 && matrix[_i, _j, 0] != -2)
                                {
                                    matrix[_i, _j, 0] = step; // Добав-
                                    matrix[_i, _j, 1] = i; // ля-
                                    matrix[_i, _j, 2] = j; // ем
                                    added = true; // Что-то добавили
                                }
                            }
                        }
                    }
                }
            }

            if(matrix[x,y,0]== -1)
            {
                result = false; // то пути не существует
            }

            if(result)
            {
                int _i=x,_j=y;

                while(matrix[_i,_j,0]!=0)
                {	
                    // записываем значение клеток и шагаем дальше к началу
                  
                    // записывать надо _i _J	
                    int li = matrix[_i,_j,1];
                    int lj = matrix[_i,_j,2];
                    ind.Add(new[] { li, lj });
                    _i=li;_j=lj;
                }
            }
            for (int i = size - 1; i >= 0; i--)
            {
                for (int j = size - 1; j >= 0; j--)
                {
                    res += (matrix[i, j, 0]+1) + "\t";
                }
                res += "\r\n";
            }
            res += "Длина пути = " + matrix[0, 0, 0];
            res += "\r\n";
            for (int i = 0; i < ind.Count; i++)
            {
                if (i < ind.Count-1)
                    res += "( " + (ind[i][0]+1) + "," + (ind[i][1]+1) + ") " + " -> ";
                else
                    res += "( " + (ind[i][0]+1) + "," + (ind[i][1]+1) + " )";
            }
            return res;
        }
    }
}
