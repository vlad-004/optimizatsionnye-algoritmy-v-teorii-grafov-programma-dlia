﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ford
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Opacity += .06;
            if (Opacity == 1) { timer1.Stop();}
        }
    }
}