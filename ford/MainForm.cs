﻿using System;
using Graph;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;

namespace ford
{
    public partial class MainForm : Form
    {
        private NumericUpDown _startPoint, _endPoint;
        private CheckBox _enableSymmetry;

        private class DijkstraData //класс данных
        {
            // для каждой вершины я буду хранить цену(длину ребра) и ту вершиу в которую пришли данные 
            public Node Previous { get; set; }

            public double Price { get; set; }
        }

        public static List<Node> Dijkstra(Graph graph, Dictionary<Edge, double> weights, Node start,
            Node end) //алгоритм будет принимать граф,веса, конечную вершину
        {
            var notVisited = graph.Nodes.ToList(); // вершины которые мы еще не посетили
            var track =
                new Dictionary<Node, DijkstraData
                >(); //завожу словарь в котором буду хранить информацию об оценках для каждой вершины
            //положу туда информацию о нулевой вершине:
            track[start] = new DijkstraData { Price = 0, Previous = null };

            while (true)
            {
                //сначала нам надо выбрать ту вершину которую мы собираемся раскрывать, я буду ее искать в списке непосещеных
                Node toOpen = null;
                var bestPrice = double.PositiveInfinity; //завожу цену
                foreach (var e in notVisited) //для каждой вершины в не посещеных:
                {
                    if (track.ContainsKey(e) && track[e].Price < bestPrice)
                    {
                        bestPrice = track[e].Price;
                        toOpen = e;
                    }
                }
                //обработаем случай когда вершина равна null, т.е. когда в нашем графе нет подходящих для раскрытия вершин
                if (toOpen == null) return null;
                if (toOpen == end) break; //выход из цикла: если найденая вершина равна конечной,тогда надо выйти 
                //иначе буду раскрывать вершину
                foreach (var e in toOpen.IncidentEdges.Where(z => z.From == toOpen)
                ) //пройду по всем ребрам у которых эта вершина является начальной(т.е.расматриваю ориентированый граф)
                {
                    //для каждого такого ребра 
                    var currentPrice =
                        track[toOpen].Price +
                        weights[e]; //возьму его текущую цену которая равна трекинку + вес соответствующего ребра
                    var nextNode = e.OtherNode(toOpen); // считаю ту вершину куда я прихожу по этому ребру 
                    if (!track.ContainsKey(nextNode) || track[nextNode].Price > currentPrice
                    ) //если для этой вершины еще не найдена вообще никакая оценка или  найденая оценка хуже чем посчитаная через раскрываемуюю вершину(нужно обновлять оценку в том случае если тарая оценка была больше текущей,тогда будетскася кратчайший путь) 
                    {
                        //то я обновлю данные следущим образом:
                        track[nextNode] =
                            new DijkstraData
                            {
                                Previous = toOpen,
                                Price = currentPrice
                            }; //поставлю сюда новую цену и так же сохраню ту вершину куда мы пришли
                    } //раскрытие вершины можно считать законченым
                }
                notVisited.Remove(toOpen); // исключу ее из списка не посещеных
            }
            //теперь для каждой песещеной вершины у нас хранится цена и откуда мы туда пришли
            var result = new List<Node>(); //заводим результат
            while (end != null)
            {
                result.Add(end);
                end = track[end].Previous;
            } // после этого разворачиваем  результат и возвращаем его
            result.Reverse();
            return result;
        }

        // фронта волны.
        public void FrontVolnLabirint()
        {
            int[,] graph = GetMatrixRastInt();
            Voln v = new Voln(graph);
            AnswerTB.Text = v.SearchWayLabirint();
        }

        public void FrontVolnGraph()
        {
            int[,] graph = GetMatrixRastInt();
            Voln v = new Voln(graph);
            AnswerTB.Text = v.SearchWayGraph((int)_startPoint.Value, (int)_endPoint.Value);
        }

        public MainForm()
        {
           
            InitializeComponent();
            openFileDialog1.Filter = saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            VertexCount_ValueChanged(null, null);
            MethodCB.SelectedIndex = 0;
        }

        //событие по изменению ячейки, заполняет симетричную относительно главной диагонали ячейку
        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (_enableSymmetry.Checked)
            {
                WeightMatrix.Rows[e.ColumnIndex].Cells[e.RowIndex].Value =
                    WeightMatrix.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            }
        }
        private double[] _h = new double[20]; //<----------------------------------------------------------------------
     
        //событие на выполнение алг. Форда:
        private void FordAlgorithm()
        {
            double[,] graph = GetMatrixRast();
            var graf = new GraphAlgoritm();
            AnswerTB.Text = "Расстояние между первой и остальными вершинами: \r\n";
            _h = graf.Ford(graph);
            for (var i = 0; i < VertexCount.Value; i++)
                {
                    if (double.IsPositiveInfinity(_h[i]))
                    {
                        var i2 = i + 1;
                        AnswerTB.Text += "\r\t\nс вершиной  " + i2 + " ,  нет пути в вершину\r\n ";
                    }
                    else
                    {
                        var i2 = i+1;
                        AnswerTB.Text += "\r\t\nс вершиной  " + i2  + " , расстояние = " + _h[i] + "\r\n";
                    }
                }
            for (int i = 1; i <= (int)VertexCount.Value; i++)
            {
                FAlgorithm(i);
            }
         }

        private void ErrorMessager(string message, string caption)
        {
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        
        //Дейкстры
        private void DijkstraAlgorithm()
        {
            for (var i = 0; i < VertexCount.Value; i++)
            {
                for (var j = 0; j < VertexCount.Value; j++)
                {
                    if ((string)WeightMatrix.Rows[i].Cells[j].Value == "---" || WeightMatrix.Rows[i].Cells[j].Value == null)
                    {
                        WeightMatrix.Rows[i].Cells[j].Value = "---";
                    }
                }
            }
            //создаем матрицу весов 
            var graph = new Graph(20); //-<---------------------------------------------------------------------------------
            var weights = new Dictionary<Edge, double>(); //заводим масив весов, и словарь для весов(для Дейкстры)
            try
            {
                for (var i = 0; i < VertexCount.Value; i++)
                {
                    for (var j = 0; j < VertexCount.Value; j++)
                    {
                        if (WeightMatrix.Rows[i].Cells[j].Value.ToString() != "---")
                        {
                            weights[graph.Connect(i, j)] = int.Parse(WeightMatrix.Rows[i].Cells[j].Value.ToString());
                        }
                    }
                }
                var path = Dijkstra(graph, weights, graph[0], graph[(int)_endPoint.Value - 1])
                    .Select(n => n.NodeNumber);
                AnswerTB.Text = "Кратчайший маршрут: ";
                int flagJ = 0, flag = 0, z = 0;
                foreach (var item in path)
                {
                    if (flag > 0)
                    {
                        var flagI = flagJ;
                        flagJ = item;
                        z += int.Parse(WeightMatrix.Rows[flagI].Cells[flagJ].Value.ToString());
                    }
                    AnswerTB.Text += (item + 1).ToString();
                    if (item != path.Last())
                    {
                        AnswerTB.Text += " --> ";
                    }
                    flag++;
                }
                AnswerTB.Text += "\r\nПри этом кратчайшее расстояние будет = " + z.ToString();
            }
            catch (Exception)
            {
                ErrorMessager("В конечный город нет пути", "Error");
            }
        }
        private void FAlgorithm(int endd)
        {
            
            //создаем матрицу весов 
            var graph = new Graph(20); //-<---------------------------------------------------------------------------------
            var weights = new Dictionary<Edge, double>(); //заводим масив весов, и словарь для весов(для Дейкстры)
            try
            {
                for (var i = 0; i < VertexCount.Value; i++)
                {
                    for (var j = 0; j < VertexCount.Value; j++)
                    {
                        if (WeightMatrix.Rows[i].Cells[j].Value.ToString() != "---")
                        {
                            weights[graph.Connect(i, j)] = int.Parse(WeightMatrix.Rows[i].Cells[j].Value.ToString());
                        }
                    }
                }
                var path = Dijkstra(graph, weights, graph[0], graph[endd - 1])
                    .Select(n => n.NodeNumber);
                AnswerTB.Text += "\r\nмаршрут к вершине "+endd+" : ";
                int flagJ = 0, flag = 0, z = 0;
                foreach (var item in path)
                {
                    if (flag > 0)
                    {
                        var flagI = flagJ;
                        flagJ = item;
                        z += int.Parse(WeightMatrix.Rows[flagI].Cells[flagJ].Value.ToString());
                    }
                    AnswerTB.Text += (item + 1).ToString();
                    if (item != path.Last())
                    {
                        AnswerTB.Text += " --> ";
                    }
                    flag++;
                }
            }
            catch (Exception)
            {
                AnswerTB.Text += "\r\n нет пути в вершину "+endd;
            }
        }


        //для красивого бэкграунда
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            using (var brush = new LinearGradientBrush(ClientRectangle,
                Color.WhiteSmoke,
                Color.SteelBlue,
                0F))
            {
                e.Graphics.FillRectangle(brush, ClientRectangle);
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }
        
        //открытие файла
        private void OpenProject_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var mystr = openFileDialog1.OpenFile();
                using (var myread = new StreamReader(mystr))
                {
                    var data = myread.ReadToEnd()
                        .Replace("\r", "")
                        .Split('\n')
                        .Where(x => !string.IsNullOrEmpty(x))
                        .Select(x => x.Split(';').Where(y => !string.IsNullOrWhiteSpace(y)).ToArray())
                        .ToArray();

                    VertexCount.Value = data.Length;

                    for (var i = 0; i < data.Length; i++)
                        for (var j = 0; j < data[i].Length; j++)
                            WeightMatrix.Rows[i].Cells[j].Value = data[i][j];
                }
            }
        }

        //сохранение в файл.
        private void SaveAs_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (var myStream = saveFileDialog1.OpenFile())
                using (var myWriter = new StreamWriter(myStream))
                {
                    for (var i = 0; i < WeightMatrix.RowCount; i++)
                    {
                        var s = "";
                       
                        for (var j = 0; j < WeightMatrix.ColumnCount; j++)
                        {
                           var rc = WeightMatrix.Rows[i].Cells[j].EditedFormattedValue.ToString();
                            if (string.IsNullOrWhiteSpace(rc))
                            {
                                rc = "---";
                            }
                            s += (rc + ";");
                        }
                        myWriter.WriteLine(s);
                    }
                }
            }
        }
        
        //событие на выполн Прима
        private void PrimaAlgorithm()
        {
            var graph = new int[20, 20];//<---------------------------------------------------------------------------
            for (var i = 0; i < VertexCount.Value; i++)
            {
                for (var j = 0; j < VertexCount.Value; j++)
                {
                    if (WeightMatrix.Rows[i].Cells[j].Value == "---" || WeightMatrix.Rows[i].Cells[j].Value == null)
                    {
                        //заполняем прочерками пустые ячейки
                        WeightMatrix.Rows[i].Cells[j].Value = "---";
                        graph[i, j] = 0;
                        //там чде --- ставим в матрицу 0 чтобы показать что пути нет
                    }
                    else
                    {
                        try
                        {
                            graph[i, j] = int.Parse(WeightMatrix.Rows[i].Cells[j].Value.ToString());
                        }
                        catch (FormatException)
                        {
                            ErrorMessager(string.Format("В клетке {0}:{1} Введенно не корректное значение", i+1, j+1), "ERROR");
                            return;
                        }
                    }
                }
            }
            var pa = new PrimsAlgirithm.PrimsAlgorithm((int)VertexCount.Value);
          

            // отображаем ребра и веса для них
            AnswerTB.Text = pa.primMST(graph);
        }
        //Алгоритм форда-беллмана
        private void FordBellmanAlgorithm()
        {
            var V = 0; //  Количество вершин в графе
            var E = 0; // Количество ребер в графе 
            for (var i = 0; i < (int)VertexCount.Value; i++)
            {
                for (var j = 0; j < (int)VertexCount.Value; j++)
                {
                    if ((string)WeightMatrix.Rows[i].Cells[j].Value == "---" || WeightMatrix.Rows[i].Cells[j].Value == null)
                    {
                        //заполняем прочерками пустые ячейки
                        WeightMatrix.Rows[i].Cells[j].Value = "---";
                    }
                    else
                    {
                        E++;
                    }
                }
            }
            var graph = new FBAlgoritm((int)VertexCount.Value, E);
            var qwe = 0;
            for (var i = 0; i < (int)VertexCount.Value; i++)
            {
                for (var j = 0; j < (int)VertexCount.Value; j++)
                {
                    if (WeightMatrix.Rows[i].Cells[j].Value == "---" || WeightMatrix.Rows[i].Cells[j].Value == null)
                    {
                        WeightMatrix.Rows[i].Cells[j].Value = "---";
                    }
                    else
                    {
                        try
                        {
                            graph.edge[qwe].Src = i;
                            graph.edge[qwe].Dest = j;
                            graph.edge[qwe].weight = int.Parse(WeightMatrix.Rows[i].Cells[j].Value.ToString());
                            qwe++;
                        }
                        catch (FormatException)
                        {
                            ErrorMessager(string.Format("В клетке {0}:{1} Введенно не корректное значение", i+1, j+1), "ERROR");
                            return;
                        }
                    }
                }
            }
            AnswerTB.Text = graph.BellmanFord(graph, (int)_startPoint.Value-1);
        }


        // Событие при изменении элемента "Количество весов", задающее размер матрицы.
        private void VertexCount_ValueChanged(object sender, EventArgs e)
        {
            WeightMatrix.Columns.Clear();
            WeightMatrix.Rows.Clear();
            for (var i = 0; i < VertexCount.Value; i++)
            {
                WeightMatrix.Columns.Add(new DataGridViewTextBoxColumn
                {
                    HeaderText = (i + 1).ToString(),
                    SortMode = DataGridViewColumnSortMode.NotSortable,
                });
            }
            for (var i = 0; i < VertexCount.Value; i++)
            {
                WeightMatrix.Rows.Add(1);
                WeightMatrix.Rows[i].HeaderCell.Value = (i + 1).ToString();
                WeightMatrix.Rows[i].Cells[i].Value = "---";
            }
        }

        // событие при ресайзе датагрида
        private void dataGridView1_Resize(object sender, EventArgs e)
        {
            WeightMatrix.Update();
            WeightMatrix.Refresh();
            WeightMatrix.Invalidate();
        }
        //для работы с доп. параметрами 
        private void MethodCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            PreferencesGB.Controls.Clear();
            _enableSymmetry = new CheckBox
            {
                Size = new Size(180, 25),
                Location = new Point(5, 18),
                Text = "Включить симметричность"
            };
            PreferencesGB.Controls.Add(_enableSymmetry);
            switch (MethodCB.SelectedIndex)
            {
                //Форд, Прима,Флойд,Волновой-лабиринт - доп. данных нет
                case 0:
                case 3:
                case 4:
                case 5:
                default:
                    break;
                //Форд-Беллман
                case 1:
                    PreferencesGB.Controls.Add(new Label
                    {
                        Size = new Size(180, 25),
                        Location = new Point(5, 45),
                        Text = "Начальная вершина алгоритма"
                    });
                    _startPoint = new NumericUpDown
                    {
                        Size = new Size(50, 30),
                        Location = new Point(190, 42),
                        Minimum = 1,
                        Maximum = VertexCount.Value,
                    };
                    PreferencesGB.Controls.Add(_startPoint);
                    break;
                //Дейкстра
                case 2:
                    PreferencesGB.Controls.Add(new Label
                    {
                        Size = new Size(180, 25),
                        Location = new Point(5, 45),
                        Text = "Конечная вершина алгоритма"
                    });
                    _endPoint = new NumericUpDown
                    {
                        Size = new Size(50, 30),
                        Location = new Point(190, 42),
                        Minimum = 1,
                        Maximum = VertexCount.Value,
                    };
                    PreferencesGB.Controls.Add(_endPoint);
                    break;
                //Волновой - для графа
                case 6:
                    PreferencesGB.Controls.Add(new Label
                    {
                        Size = new Size(180, 15),
                        Location = new Point(5, 45),
                        Text = "Начальная вершина алгоритма"

                    });
                    _startPoint = new NumericUpDown
                    {
                        Size = new Size(50, 30),
                        Location = new Point(190, 42),
                        Minimum = 1,
                        Maximum = VertexCount.Value,
                    };
                    PreferencesGB.Controls.Add(_startPoint);
                    PreferencesGB.Controls.Add(new Label
                    {
                        Size = new Size(180, 15),
                        Location = new Point(5, 67),
                        Text = "Конечная вершина алгоритма"
                    });
                    _endPoint = new NumericUpDown
                    {
                        Size = new Size(50, 30),
                        Location = new Point(190, 64),
                        Minimum = 1,
                        Maximum = VertexCount.Value,
                    };
                    PreferencesGB.Controls.Add(_endPoint);
                    break;
            }
        }

        //кнопка отчисти данных
        private void ClearBtn_Click_1(object sender, EventArgs e)
        {
            VertexCount.Value = 10;
            AnswerTB.Text = "";
            for (var i = 0; i < WeightMatrix.RowCount; i++)
            {
                for (var j = 0; j < WeightMatrix.ColumnCount; j++)
                {
                    WeightMatrix.Rows[i].Cells[j].Value = i != j ? null : "---";
                }
            }
        }

        //привязка решений алгоритмов к выпадающему списку dropdownlist
        private void SolveBtn_Click(object sender, EventArgs e)
        {
            switch (MethodCB.SelectedIndex)
            {
                case 0:
                    FordAlgorithm();
                    break; //Форд
                case 1:
                    FordBellmanAlgorithm();
                    break; //Форд-Беллман
                case 2:
                    DijkstraAlgorithm();
                    break; //Дейкстра
                case 3:
                    PrimaAlgorithm();
                    break; //Прима
                case 4:
                    FloydWarshall();
                    break; //Флойд-Уоршел
                case 5:
                    FrontVolnLabirint();
                    break; //Волновой - для лабиринта
                case 6:
                    FrontVolnGraph();
                    break; //Волновой -ддля графа
                default:
                    break;
            }
        }

        private int[,] GetMatrixRastInt()
        {
            int[,] graph = new int[(int)VertexCount.Value, (int)VertexCount.Value];
            for (var i = 0; i < (int)VertexCount.Value; i++)
            {
                for (var j = 0; j < (int)VertexCount.Value; j++)
                {
                    if ((string)WeightMatrix.Rows[i].Cells[j].Value == "1" )
                    {
                        graph[i, j] = 1;
                    }
                    else
                    {
                        try
                        {
                            graph[i, j] = 0;
                        }
                        catch (FormatException)
                        {
                            ErrorMessager(string.Format("В клетке {0}:{1} Введенно не корректное значение", i + 1, j + 1), "ERROR");
                            return null;
                        }
                    }
                }
            }
            return graph;
        }

        private double[,] GetMatrixRast()
        {
            double[,] graph = new double[(int)VertexCount.Value, (int)VertexCount.Value];
            for (var i = 0; i < (int)VertexCount.Value; i++)
                {
                    for (var j = 0; j < (int)VertexCount.Value; j++)
                    {
                        if (i == j)
                        {
                            WeightMatrix.Rows[i].Cells[j].Value = "---";
                            graph[i, j] = 0;
                        }
                        else if ((string)WeightMatrix.Rows[i].Cells[j].Value == "---" || WeightMatrix.Rows[i].Cells[j].Value == null)
                        {
                            //заполняем прочерками пустые ячейки
                            WeightMatrix.Rows[i].Cells[j].Value = "---";
                            graph[i, j] = double.PositiveInfinity;
                         }
                        else
                        {
                            try
                            {
                                graph[i, j] = double.Parse(WeightMatrix.Rows[i].Cells[j].Value.ToString().Replace('.', ','));
                            }
                            catch (FormatException)
                            {
                                ErrorMessager(string.Format("В клетке {0}:{1} Введенно не корректное значение", i + 1, j + 1), "ERROR");
                                return null;
                            }
                        }
                    }
                }
            return graph;
        }

        /// <summary>
        /// Алгоритм Флойда-Уоршелла
        /// </summary>
        /// <param name="graph">Матрица смежности</param>
        void FloydWarshall()
        {
            double[,] graph = GetMatrixRast();
            double[,] path = new double[(int)VertexCount.Value, (int)VertexCount.Value];      
            int vNum = graph.GetLength(0);
            var dist = (double[,]) graph.Clone();
            string result = String.Empty;
            string result1 = String.Empty;
            for (int k = 0; k < vNum; k++)
            {
                for (int i = 0; i < vNum; i++)
                {
                    for (int j = 0; j < vNum; j++)
                    {
                        double min = Math.Min(dist[i, j], dist[i, k] + dist[k, j]);
                        if (dist[i, j] > min)
                        {
                            dist[i, j] = min;
                        }
                     }
                }
            }

            //Вывод результата
            result += "Матрица длин кратчайших путей между каждой парой вершин графа\r\n\n";
            result += "\r\nв вершину ---->";
            for (int i = 0; i < (int)VertexCount.Value; i++)
            {
                result += "\r\t"+(i+1);
            }
            result += "\r\n\r\n";
           
            for (int i = 0; i < dist.GetLength(0); i++)
            {
                
                for (int j = 0; j < dist.GetLength(1); j++)
                {
                   
                    if (j == 0)

                        result +="из вершины "+ (i+1) + " -> \r\t" + dist[i, j] + "\r\t";
                    else
                        result += dist[i, j] + "\t";
                 
                }
                result += "\r\n";
            }
           AnswerTB.Text = result + "\r\n" + result1;
        }



        //перезапуск приложения
        private void ResetProject_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.EnableVisualStyles();
            About second = new About();
            DateTime end = DateTime.Now + TimeSpan.FromSeconds(5);
            second.Show();
        }

        private void вызовСправкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Help.ShowHelp(this, @".\\Resources\\first.chm");
        
        }
    }
}