﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ford
{
    class PrimsAlgirithm
    {
        public class PrimsAlgorithm
        {
            // Количество вершин
            public int vertices;

            // Конструктор
            public PrimsAlgorithm(int vertices)
            {
                this.vertices = vertices;
            }

            //                                      MST - минимальное остово дерево
            //Функция полезности, чтобы найти вершину с минимальным значением ключа, из набора вершин, еще не включенных в MST 
           
            public int minKey(int[] key, bool[] mstSet)
            {
                // инициализация минимального значения
                var min = int.MaxValue;
                var minIndex = -1;

                for (var v = 0; v < vertices; v++)
                {
                    if (!mstSet[v] && key[v] <= min)
                    {
                        min = key[v];
                        minIndex = v;
                    }
                }

                return minIndex;
            }

            
            //Функция полезности для отображения сконструированного MST, хранящегося в родительском []
            public string printMST(int[] parent, int n, int[,] graph)
            {
                string abc = "";
                var dfd = 0;
                abc += "Дуги используемые в алгоритме: \r\n";
                for (var i = 1; i < vertices; i++)
                {
                    abc += "\r\nиз вершины " + (parent[i] + 1) + " в вершину " + (i + 1) + ", вес ребра = " +
                           graph[i, parent[i]] + "\n ";
                    dfd += graph[i, parent[i]];
                }
                abc += "\r\nВес минимального остова = " + dfd;
                return abc;
            }

            
            //Функция для построения и тображения MST для графа, представленного с использованием представления матрицы смежности
            public string primMST(int[,] graph)
            {
                // Массив для хранения построенных  элементов MST
                int[] parent = new int[vertices];

                // Key values used to pick minimum weight edge in cut
                //Ключевые значения, используемые для выбора минимального веса в разрезе -????
                int[] key = new int[vertices];

                // Представить набор вершин, еще не включенных в MST
                bool[] mstSet = new bool[vertices];

                // Инициализировать все ключи как INFINITE
                for (var i = 0; i < vertices; i++)
                {
                    key[i] = int.MaxValue;
                    mstSet[i] = false;
                }

                // Always include first 1st vertex in MST.- Всегда включайте сначала первую вершину в MST.
                // Сделаем ключ 0 так,чтобы эта вершина была выбрана в качестве первой вершины
                key[0] = 0;

                // First node is always root of MST - Первая вершина всегда является корневой в МSТ ???????????
                parent[0] = -1;

                // MST будет иметь вершины - 1 ребра
                for (var count = 0; count < vertices - 1; count++)
                {
                    // Выберем минимальный ключ  вершины из набора вершин, еще не включенных в MST
                    var u = minKey(key, mstSet);

                    // Добавить выбранную вершину в набор MST
                    mstSet[u] = true;

                    // Обновить значение ключа и родительский индекс смежных вершин выбранной вершины. 
                    //Рассмотрим только те вершины, которые еще не включены в MST.
                    for (var v = 0; v < vertices; v++)
                    {
                        //  Graph [u] [v] не равен нулю только для смежных вершин m
                        // MstSet [v] неверно для вершин, еще не включенных в MST
                        
                        //Обновите ключ, только если граф [u] [v] меньше, чем ключ [v]
                        if (graph[u, v] != 0 && !mstSet[v] && graph[u, v] < key[v])
                        {
                            parent[v] = u;
                            key[v] = graph[u, v];
                        }
                    }
                }
                string abc = "";
                //int dfd = 0;
                // Отобразим построенную MST
                //abc=printMST(parent, vertices, graph);
                abc = printMST(parent, vertices, graph);
                return abc;
            }
        }
    }
}